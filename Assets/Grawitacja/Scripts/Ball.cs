﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
    public int count;
    Rigidbody2D rb;
    public float force;
    GameController gc;
    public GameObject ballprefab;
    public bool isDestroyed;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        gc = GameObject.Find("Main Camera").GetComponent<GameController>();
	}
	
    //Check ball if it is build from 50 other balls
    public void CheckBall() {
        if(count >= 50) {
            DestroyBall();
        }
    }

    IEnumerator ChangeBackColliders(GameObject go) {
        
        yield return new WaitForSeconds(0.5f);
        go.GetComponent<CircleCollider2D>().enabled = true;
        go.transform.GetChild(0).GetComponent<CircleCollider2D>().enabled = true;
        Destroy(gameObject);
    }

    //Destroy ball in 50 pieces
    void DestroyBall() {
        for(int i = 0; i < 50; i++) {
            GameObject nb = Instantiate(ballprefab, transform.position, Quaternion.identity) as GameObject;
            nb.transform.localScale = new Vector3(1, 1, 1);
            nb.GetComponent<CircleCollider2D>().enabled = false;
            nb.transform.GetChild(0).GetComponent<CircleCollider2D>().enabled = false;
            StartCoroutine(ChangeBackColliders(nb));
            nb.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-5,5), Random.Range(-5, 5)) * 5, ForceMode2D.Impulse);     
        }
        gameObject.transform.position = new Vector3(Random.Range(1000, 10000000000), Random.Range(1000, 10000000000), 0);
    }

    void OnCollisionEnter2D(Collision2D col) {
        if(col.gameObject.tag == "Ball") {
            gc.DestroyBalls(gameObject, col.gameObject);

        }
    }

    void OnTriggerStay2D(Collider2D col) {
        if (col.tag == "Collision") {
            if (col) {
                if (gc.gravitation)
                    rb.AddForce(-(col.transform.parent.transform.position - transform.position) * col.transform.parent.GetComponent<Ball>().force, ForceMode2D.Force);
                else
                    rb.AddForce((col.transform.parent.transform.position - transform.position) * col.transform.parent.GetComponent<Ball>().force, ForceMode2D.Force);
            }
                
        }
    }
}
