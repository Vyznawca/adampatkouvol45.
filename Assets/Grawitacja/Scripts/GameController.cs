﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
    public static int ballsCount;
    public GameObject ballPrefab;
    public Text textObject;
    Queue<GameObject> queue;
    public bool gravitation;
    // Use this for initialization
    void Start () {
        InvokeRepeating("SpawnBall", 0, 0.25f);
        queue = new Queue<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

   
    void SpawnBall() {
        if(ballsCount < 250){ 
            
            
            //if (queue.Count <= 0) {
                
                Instantiate(ballPrefab, new Vector3(Random.Range(-60, 60), Random.Range(-39, 39), 0), Quaternion.identity);
                ballsCount++;
                textObject.text = "Balls count: " + ballsCount.ToString();
            //}
            //else {
            //    GameObject go = queue.Dequeue();
            //    go.SetActive(true);
            //    go.transform.position = new Vector3(Random.Range(-60, 60), Random.Range(-39, 39), 0);
            //    ballsCount++;
            //    textObject.text = "Balls count: " + ballsCount.ToString();
            //    go.transform.localScale = new Vector3(1, 1, 1);



            //}Chciałem dać object pooling ale coś się psuje i nie zdążyłem naprawić
            
        }
        else {
            gravitation = true;
        }
    }
    
    //Function that destroys both balls and create a new, bigger one
    public void DestroyBalls(GameObject g1, GameObject g2) {
        Ball b1 = g1.GetComponent<Ball>();
        Ball b2 = g2.GetComponent<Ball>();
        Transform temp;
        if (g1.transform.localScale.x > g2.transform.localScale.x) {
            temp = g1.transform;
        } 
        else {
            temp = g2.transform;
        }

        if (!b1.isDestroyed && !b2.isDestroyed) {
    
            
            GameObject nb = Instantiate(ballPrefab, temp.position , Quaternion.identity) as GameObject;
            nb.transform.localScale = temp.localScale * 1.1f;
            nb.GetComponent<Ball>().count = Mathf.Max(b1.count, b2.count) + 1;
            nb.GetComponent<Ball>().CheckBall();
            b1.isDestroyed = true;
            
        }
        else {
            if (g1) {
                b1.isDestroyed = true;
                if(!queue.Contains(g1))
                    queue.Enqueue(g1);
                g1.SetActive(false);
            }

            if (g2) {
                
                b2.isDestroyed = true;
                if (!queue.Contains(g2))
                    queue.Enqueue(g2);
                g2.SetActive(false);
            }

        }

    }
}
