﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public GameObject ammunitionObject;
    public Ammunition ammunitionS;
    public float maxStrengthOfShoot;
    public float maxTensionLength;

    public static StateOfShooting stateOfShooting;
	// Use this for initialization
	void Start () {
        stateOfShooting = StateOfShooting.Ready;
        ammunitionS = ammunitionObject.GetComponent<Ammunition>();
    }
	
	// Update is called once per frame
	void Update () {
	    if(stateOfShooting == StateOfShooting.Ready) {
            if (Input.GetMouseButtonDown(0))
                stateOfShooting = StateOfShooting.Targeting;
        }
        else if(stateOfShooting == StateOfShooting.Targeting) {
            Vector2 newPos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector3.Distance(ammunitionS.startPosition, newPos) < maxTensionLength)
                ammunitionObject.transform.position = newPos;
            else {
                ammunitionObject.transform.position = newPos.normalized * maxTensionLength;
            }

            if (Input.GetMouseButtonUp(0)) {
                stateOfShooting = StateOfShooting.Shot;
                Shoot();
            }
        }
        else {

        }

    }

    void Shoot() {
        Vector3 force = -(Vector3.Distance(ammunitionS.startPosition, ammunitionObject.transform.position) / maxTensionLength) * maxStrengthOfShoot * ammunitionObject.transform.position;
        ammunitionObject.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
        ammunitionObject.GetComponent<Rigidbody2D>().gravityScale = 1;
        ammunitionObject.GetComponent<Ammunition>().Invoke("DestroyAmmunition", 1);
    }
}

public enum StateOfShooting {
    Ready,
    Targeting,
    Shot
}