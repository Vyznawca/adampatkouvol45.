﻿using UnityEngine;
using System.Collections;

public class Ammunition : MonoBehaviour {
    public Vector3 startPosition;
	// Use this for initialization
	void Start () {
        startPosition = transform.position;

    }

    public void DestroyAmmunition() {
        transform.position = startPosition;
        PlayerController.stateOfShooting = StateOfShooting.Ready;
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        rb.velocity = Vector3.zero;
    }
}
